import React from 'react';
import ReactDOM from 'react-dom';
import TimesTable from './TimesTable/TimesTable';

ReactDOM.render(
    <TimesTable tableSize={13} />,
    document.getElementById('root'),
);
